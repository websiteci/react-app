## Descripción del Bug

**Descripción:**
Una breve descripción del problema.

**Pasos para reproducir:**
1. Paso detallado 1.
2. Paso detallado 2.
3. Paso detallado 3.
4. ...

**Comportamiento Esperado:**
Describe lo que debería ocurrir si el bug no existiera.

**Comportamiento Actual:**
Describe lo que realmente está ocurriendo.

## Información Adicional

**Ambiente:**
- **Sistema Operativo:** (Ej. Windows 10, macOS 12.1, Ubuntu 20.04)
- **Navegador/Versión:** (Ej. Chrome 91, Firefox 89)
- **Versión de la Aplicación:** (Ej. 1.0.0)
- **Hardware:** (Opcional, si el bug depende del hardware)

**Archivos Adjuntos:**
- Capturas de pantalla.
- Archivos de registro.
- Otros archivos relevantes.

## Notas

**Notas Adicionales:**
Cualquier otra información que pueda ser relevante para resolver el problema.

**Posibles Soluciones:**
Si tienes alguna idea de cómo solucionar el problema, descríbela aquí.

## Seguimiento

**Responsable:**
La persona asignada para resolver este bug.

**Estado:**
- [ ] Abierto
- [ ] En progreso
- [ ] Resuelto
- [ ] Cerrado